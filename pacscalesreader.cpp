//
// Reads data from a Pacific Scales device.
//
// Data is read automatically using a thread and can be accessed using getLatestReading().
//

#include <string>
#include <vector>
#include <fstream>
#include <mutex>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <algorithm>
#include <map>
#include <iostream>

#include "pacscalesreader.hpp"
#include "fdstream.hpp"


// Constructor. Takes a serial device file name as an argument.
PacScalesReader::PacScalesReader(const std::string &serialDevicePath)
    : m_fd(-1),
      m_serialStream(-1),
      m_isOpen(false),
      m_total(0.0),
      m_gotData(false)
{
    // Open the device.
    if (!openDevice(serialDevicePath))
        return;

    // Start the reader thread.
    m_readerThread = std::thread(&PacScalesReader::reader, this);
}


// Destructor.
PacScalesReader::~PacScalesReader()
{
    // Close the serial device.
    if (m_fd >= 0)
    {
        ::close(m_fd);
    }

    m_isOpen = false;

    // Join the thread.
    m_readerThread.join();
}


// Open the serial device and set termio modes.
bool PacScalesReader::openDevice(const std::string &serialDevicePath)
{
    // Open the serial port.
    m_serialDevicePath = serialDevicePath;
    m_fd = ::open(serialDevicePath.c_str(), O_RDWR);
    if (m_fd < 0)
    {
        std::cerr << "can't open serial port " << serialDevicePath << ": " << strerror(errno) << std::endl;
        m_isOpen = false;
        return false;
    }

    // Set termio modes.
    setSerialModes();

    // Convert it into an istream.
    m_serialStream = fdistream(m_fd);
    m_isOpen = true;

    return true;
}


// Sets the termio modes on the serial device to allow line-by-line reading.
bool PacScalesReader::setSerialModes()
{
    // Set the serial modes.
    struct termios tio;
    memset(&tio, 0, sizeof(tio));

    if (tcgetattr(m_fd, &tio) != 0)
    {
        std::cerr << "can't get serial port attrs for " << m_serialDevicePath << ": " << strerror(errno) << std::endl;
        return false;
    }

    tio.c_cflag |= CS8;       // 8 data bits.
    tio.c_cflag &= ~PARENB;   // No parity.
    tio.c_cflag &= ~CSTOPB;   // One stop bit.
    tio.c_cflag &= ~CRTSCTS;  // No hardware flow control.
    tio.c_cflag |= CREAD | CLOCAL;  // Don't use modem control lines.
    tio.c_lflag &= ~ICANON;   // No input processing.
    tio.c_lflag &= ~ECHO;     // No echo.
    tio.c_lflag &= ~ECHOE;    // No erasure.
    tio.c_lflag &= ~ECHONL;   // No newline echo.
    tio.c_lflag &= ~ISIG;     // No INTR, QUIT and SUSP.
    tio.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off software flow control.
    tio.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL); // Disable special character handling on receive.
    tio.c_oflag &= ~OPOST;    // Disable special interpretation of output characters.
    tio.c_oflag &= ~ONLCR;    // Prevent conversion of newlines to carriage returns.
    tio.c_cc[VTIME] = 0;      // Return bytes as soon as they're available.
    tio.c_cc[VMIN] = 1;
    cfsetispeed(&tio, B2400); // Baud rate = 2400bps
    cfsetospeed(&tio, B2400);

    tcflush(m_fd, TCIFLUSH);
    if(tcsetattr(m_fd, TCSAFLUSH, &tio) < 0)
    {
        std::cerr << "can't set terminal settings on " << m_serialDevicePath << ": " << strerror(errno) << std::endl;
        return false;
    }

    return true;
}


// The reader thread body.
void PacScalesReader::reader()
{
    // A local copy of the sensor data we're collecting.
    std::map<std::string, double> sensors;
    double total = 0;

    // Keep going until the file is closed, which will happen if someone calls our destructor.
    while (!m_serialStream.eof() && m_isOpen)
    {
        // Read a line of input at a time.
        char lineBuf[MAX_PAC_LINE];
        if (!m_serialStream.getline(lineBuf, MAX_PAC_LINE))
            return;

        std::string line = lineBuf;

        // Remove trailing CR and all spaces to avoid issues with spaces between '-' and the sensor value.
        line.erase(remove_if(line.begin(), line.end(), ::isspace), line.end());

        // Handle each type of line.
        if (line == "/")
        {
            // Packet start.
            sensors.clear();
            total = 0;
        }
        else if (line == "\\")
        {
            // Packet end - put the results in the result area.
            {
                std::lock_guard<std::mutex> lock(m_resultsMutex);
                m_readings = sensors;
                m_total = total;
                m_gotData = true;
            }

            sensors.clear();
            total = 0;
        }
        else
        {
            // Remove any trailing "Kg".
            if (line.length() >= 2 && line.substr(line.length() - 2) == "Kg")
            {
                line = line.substr(0, line.length() - 2);
            }

            // Check it's a payload line - should have a colon.
            auto colonPos = line.find(':');
            if (colonPos != std::string::npos)
            {
                std::string fieldName = line.substr(0, colonPos);
                double value = std::stod(line.substr(colonPos+1).c_str());

                if (fieldName == "TOTAL")
                {
                    // Take note of the total.
                    total = value;
                }
                else
                {
                    // Accumulate sensor readings in a map.
                    sensors[fieldName] = value;
                }
            }
        }
    }
}


// Get the most recent reading.
bool PacScalesReader::getLatestReading(std::map<std::string, double> &readings, double &total)
{
    // Thread-safe access to the most recent results.
    std::lock_guard<std::mutex> lock(m_resultsMutex);

    readings = m_readings;
    total = m_total;

    return m_gotData;
}
