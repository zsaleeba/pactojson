SRCS    = pactojson.cpp pacscalesreader.cpp
OBJS    = $(SRCS:.cpp=.o)
TARGET  = pactojson
LIBS	= -lpthread
CXXFLAGS = -std=c++11 -Wall -Wextra -Werror -g

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJS) $(LIBS)

clean:
	rm -f $(TARGET) $(OBJS)

.PHONY: clean all
