//
// pactojson converts a stream of serial data in Pacific Scales format into
// JSON data which is sent to stdout every ten seconds, synchronised to each
// ten second wall clock period.
//
// It consists of two main parts:
//
//    * pacscalesreader.cpp/hpp - a class which runs as a thread, reading 
//                      data from a serial line and parsing the data as it 
//                      comes in.
//    * pactojson.cpp - the main program which waits for the appointed times
//                      to check the data and output it in JSON.
//
// It uses two externally obtained libraries, both of which are single header
// files included here for convenience:
// 
//    * json.hpp     - JSON library. Used here for outputting JSON.
//    * fdstream.hpp - a C++ stream which can be initialised from a file 
//                     descriptor. Used here to read the serial data as a 
//                     stream.
//
// Written by Zik Saleeba 2021-02-10
//

#include <iostream>
#include <cstdlib>
#include <map>
#include <string>
#include <chrono>
#include <ctime>

#include "pacscalesreader.hpp"
#include "json.hpp"


// Report JSON every 10s.
static const int REPORTING_PERIOD = 10;

// Tab width to use when pretty-printing JSON.
static const int JSON_TAB_WIDTH = 4;


//
// Wait until the desired time division is reached (in seconds).
//

void waitForTimeDivision(int period)
{
    // Get the time in fields.
    auto now = std::chrono::system_clock::now();
    std::time_t nowTime = std::chrono::system_clock::to_time_t(now);
    struct tm *nowTm = localtime(&nowTime);

    // Get the time of the previous time step.
    struct tm lastTimeStepTm = *nowTm;
    lastTimeStepTm.tm_sec = lastTimeStepTm.tm_sec - lastTimeStepTm.tm_sec % period;
    std::time_t lastTimeStepTime = mktime(&lastTimeStepTm);

    // Work out when we should wait until.
    std::time_t nextTimeStepTime = lastTimeStepTime + period;

    // Wait until then.
    std::this_thread::sleep_until(std::chrono::system_clock::from_time_t(nextTimeStepTime));
}


//
// Report the results to stdout in JSON format.
//

void jsonWrite(const std::map<std::string, double> &readings, double receivedTotal)
{
    // Calculate our own total of the the sensor readings.
    double localTotal = 0.0;
    for (auto r: readings)
    {
        localTotal += r.second;
    }

    bool valid = localTotal == receivedTotal;

    // Output the JSON.
    nlohmann::json json;
    json = readings;
    json["TOTAL"] = receivedTotal;
    json["VALID"] = valid;
    std::cout << json.dump(JSON_TAB_WIDTH) << std::endl;
}


//
// The main program. Reads the argument and starts processing data.
//

int main(int argc, char **argv)
{
    // Check command line arguments.
    if (argc != 2)
    {
        std::cerr << "Format: pactojson <serial device path>" << std::endl;
        return EXIT_FAILURE;
    }

    // Start reading data from the serial port.
    PacScalesReader reader(argv[1]);

    while (true)
    {
        // Wait for the next 10s interval.
        waitForTimeDivision(REPORTING_PERIOD);

        // Get an update.
        std::map<std::string, double> readings;
        double total;
        bool gotReading = reader.getLatestReading(readings, total);
        if (gotReading)
        {
            // Output to JSON.
            jsonWrite(readings, total);
        }
    }

    return EXIT_SUCCESS;
}
