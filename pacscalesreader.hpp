//
// Reads data from a Pacific Scales device.
//
// Data is read automatically using a thread and can be accessed using getLatestReading().
//

#ifndef PAC_SCALES_READER_HPP
#define PAC_SCALES_READER_HPP

#include <string>
#include <vector>
#include <fstream>
#include <mutex>
#include <thread>
#include <map>

#include "fdstream.hpp"


//
// Reads data from a Pacific Scales device.
//
// Data is read automatically using a thread and can be accessed using getLatestReading().
//

class PacScalesReader
{
public:
    // Constants.
    static const int MAX_PAC_LINE = 1024;

public:
    // Constructor. Takes a serial device file name as an argument.
    PacScalesReader(const std::string &serialDevice);

    // Destructor.
    ~PacScalesReader();

    // Check if the device opened successfully.
    bool isOpen() const { return m_isOpen; }

    // Get the most recent reading.
    bool getLatestReading(std::map<std::string, double> &readings, double &total);

private:
    // Opens the serial device.
    bool openDevice(const std::string &serialDevicePath);

    // Set up the termio modes on the serial port.
    bool setSerialModes();

    // The reader thread body.
    void reader();

private:
    // Serial port.
    std::string         m_serialDevicePath;
    int                 m_fd;
    fdistream           m_serialStream;
    bool                m_isOpen;

    // Reader thread.
    std::thread         m_readerThread;

    // Readings and totals which can be accessed publicly.
    std::map<std::string, double> m_readings;
    double              m_total;
    bool                m_gotData;
    std::mutex          m_resultsMutex;

};

#endif // PACSCALES_READER_HPP
