# pactojson

pactojson converts a stream of serial data in Pacific Scales format into
JSON data which is sent to stdout every ten seconds, synchronised to each
ten second wall clock period.

It consists of two main parts:

   * pacscalesreader.cpp/hpp - a class which runs as a thread, reading 
                     data from a serial line and parsing the data as it 
                     comes in.
   * pactojson.cpp - the main program which waits for the appointed times
                     to check the data and output it in JSON.

It uses two externally obtained libraries, both of which are single header
files included here for convenience:
 
   * json.hpp     - JSON library. Used here for outputting JSON.
   * fdstream.hpp - a C++ stream which can be initialised from a file 
                    descriptor. Used here to read the serial data as a 
                    stream.

Written by Zik Saleeba 2021-02-10
